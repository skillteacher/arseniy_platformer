using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private int coinCost = 1;
    [SerializeField] private string playerTag = "Player";
    private Game game;
    private bool isCollected = false;

    private void Start()
    {
        game = FindObjectOfType<Game>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isCollected  || game == null) return;
        if (!collision.CompareTag(playerTag)) return;
        isCollected = true;
        game.addCoins(coinCost);
        Destroy(gameObject);
    }


}