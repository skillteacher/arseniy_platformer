using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Danger : MonoBehaviour
{
    [SerializeField] private int damage = 1;
    [SerializeField] private float delay = 1f;
    private bool alreadyHit = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Health health = collision.GetComponent<Health>();
        if (alreadyHit || health == null) return;
        health.TakeDamage(damage);
        alreadyHit = true;
        StartCoroutine(Wait(delay));
    }

    private IEnumerator Wait(float amount)
    {
        yield return new WaitForSeconds(amount);
        alreadyHit = false;

    }
}
