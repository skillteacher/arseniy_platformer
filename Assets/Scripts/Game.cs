using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField] private GameObject restartMenu;
    [SerializeField] private TMP_Text finalScore;
    [SerializeField] private string firstLevelName;
    [SerializeField] private musicGameOver audioSwitch;
    private int coinCount = 0;
    private int currentMaxLives;

    private void Awake()
    {
        Game[] anotherGameScript = FindObjectsOfType<Game>();
        if(anotherGameScript.Length>1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            restartMenu.SetActive(false);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void addCoins(int amount)
    {
        coinCount += amount;
        PlayerUI.ui.SetCoins(coinCount);
    }

    public void GameOver(int maxLives)
    {
        Time.timeScale = 0f;
        restartMenu.SetActive(true);
        audioSwitch.SwitchMusic();
        finalScore.text = coinCount.ToString();
        currentMaxLives = maxLives;
    }

    public void RestartGame()
    {
        coinCount = 0;
        PlayerUI.ui.SetCoins(coinCount);
        PlayerUI.ui.SetLives(currentMaxLives);
        SceneManager.LoadScene(firstLevelName);
        restartMenu.SetActive(false);
        Time.timeScale = 1f;
    }

}
