using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
   
    [SerializeField] private float speed = 10f;
    [SerializeField] private KeyCode jumpButton = KeyCode.Space;
    [SerializeField] private float jumpForce = 2f; 

    private Rigidbody2D rb;
    private bool isGrounded;
    private Animator animator;
    private bool isTurnedR = true;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }
    
    private void Update()
    {
        var direction = Input.GetAxis("Horizontal");
        if (isGrounded) MoveHorizontaly(direction);
        
        if(Input.GetKeyDown(jumpButton) && isGrounded)
        {
            Jump();
        }

        RunAnimation();
        Flip(direction);

    } 

    
    private void MoveHorizontaly(float direction)
    {
        Vector2 velocity = new Vector2(direction * speed, rb.velocity.y);
        rb.velocity = velocity;
    }

    private void Jump()
    {
        Vector2 velocity = new Vector2(rb.velocity.x, jumpForce);
        rb.velocity = velocity;
        isGrounded = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isGrounded = true;
    }

    private void RunAnimation()
    {
        bool isMove = Mathf.Abs(rb.velocity.x) > 0f;
        animator.SetBool("run", isMove);    
    } 
    
    private void Flip(float direction)
    {
        if (direction == 0f) return;
        bool isDirectionRight = direction > 0f;
        if (direction > 0f == isTurnedR) return;
        Vector3 scale = transform.localScale;
        transform.localScale = new Vector3(-1f * scale.x, scale.y, scale.z);
        isTurnedR = !isTurnedR;
    }

}
