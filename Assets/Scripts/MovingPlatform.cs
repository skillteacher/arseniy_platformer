using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private Vector3 startPoint;
    [SerializeField] private Vector3 finishPoint;
    [SerializeField] private float distance = 0.5f;

    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        Vector3 direction = (finishPoint - startPoint).normalized;
        rb.velocity = direction * speed;
        if(Vector3.Distance(transform.position, finishPoint)< distance)
        {
            Vector3 point = startPoint;
            startPoint = finishPoint;
            finishPoint = point;
        }

    }
}
