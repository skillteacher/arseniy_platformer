using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class musicGameOver : MonoBehaviour
{
    [SerializeField] private AudioClip gameoverMusic;
    [SerializeField] private AudioSource audioSource;

    public void SwitchMusic()
    {
        audioSource.Stop();
        audioSource.clip = gameoverMusic;
        audioSource.Play();
    }
}
